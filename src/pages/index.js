import React from 'react';
import { Layout, SEO } from 'components/common';
import Intro from 'components/landing/Intro/index';
import Experience from 'components/landing/Experience/index';
import Skill from 'components/landing/Skills/index';

export default () => (
  <Layout>
    <SEO />
    <Intro />
    <Skill />
    <Experience />
  </Layout>
);
