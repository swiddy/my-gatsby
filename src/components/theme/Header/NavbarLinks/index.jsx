import React from 'react';
import AnchorLink from 'react-anchor-link-smooth-scroll';
import { BrowserRouter, Route } from 'react-router-dom';
import { Wrapper } from './styles';

// eslint-disable-next-line react/prop-types
const NavbarLinks = ({ desktop }) => (
  <Wrapper desktop={desktop}>
    <a href="https://www.linkedin.com/in/suko-widodo-a27318109/">Linkedin</a>
    <a href="https://github.com/sukowidodo">Github</a>
    <a href="https://t.me/juruketikkerajaan">Telegram</a>
    <a href="https://medium.com/@sukowidodo">Articles</a>
    {/* <AnchorLink href="#projects">Projects</AnchorLink> */}
  </Wrapper>
);

export default NavbarLinks;
