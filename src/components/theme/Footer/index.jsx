import React from 'react';
import { Container } from 'components/common';

import { Wrapper, Flex, Links, Details } from './styles';

export const Footer = () => (
  <Wrapper>
    <Flex as={Container}>
      <span>
        © All rights are reserved | {new Date().getFullYear()} || Builth with Gatsby
        {/* | Made with{' '}
          <span aria-label="love" role="img">
            💖
          </span>{' '}
          by{' '}
          <a href="https://smakosh.com/?ref=portfolio-dev" rel="noopener noreferrer" target="_blank">
            Smakosh
          </a> */}
      </span>
    </Flex>
  </Wrapper>
);
