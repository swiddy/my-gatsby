import React from 'react';
import { Container } from 'components/common';
import { Label } from 'semantic-ui-react';
import { Wrapper, Exp } from './styles';

const randomColor = require('randomcolor');

const NumberList = function(props) {
  const { numbers } = props;

  const listItems = numbers.map(number => (
    <Label size="big" color="blue">
      {number}
    </Label>
  ));
  return <div>{listItems}</div>;
};

const skill = [
  'Android Development',
  'IOS Development',
  'Graphql',
  'Java',
  'Kotlin',
  'Swift',
  'Codeigniter',
  'Laravel',
  'Docker',
  'NodeJs',
  'Golang',
  'Microcontroller',
  'Embedded system (IoT)',
  'Computer Network',
];

const styles = {
  large: {
    fontSize: 60,
    fontWeight: 'bold',
  },
  small: {
    opacity: 0.7,
    fontSize: 16,
  },
};

class Skill extends React.Component {
  render() {
    return (
      <Wrapper as={Container}>
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.css"></link>
        <h1>SKILL</h1>
        <div>
          <NumberList numbers={skill} />
        </div>
      </Wrapper>
    );
  }
}

export default Skill;
