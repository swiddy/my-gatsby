import React from 'react';
import AnchorLink from 'react-anchor-link-smooth-scroll';
import { Header } from 'components/theme';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faCoffee } from '@fortawesome/free-solid-svg-icons';
import { Container, Button } from 'components/common';
import dev from 'assets/illustrations/suko.jpg';
import { Wrapper, IntroWrapper, Details, Thumbnail } from './styles';

const mystyle = {
  width: '300px',
  height: '300px',
  borderRadius: '50%',
  margin: '10px',
};

class Intro extends React.Component {
  render() {
    return (
      <Wrapper>
        <Header />
        <IntroWrapper as={Container}>
          <Details>
            <h1>Hi There!</h1>
            <h4></h4>
            <p>
              Hi i am professional fullstack mobile developer with experience more than 5 years. I have experience in developing and designing iOS, android, web and desktop apps, responsive websites.
            </p>
            <p>
              I have passion about programming, microcontroller, IOT and all about the internet technology, especially about linux and open source world.
            </p>
            <p>
             Feel free and send me a personal message if you would like to discuss you project with me.
           </p>
          </Details>
          <Thumbnail>
            <img src={dev} style={mystyle} alt="I’m Suko Widodo" />
          </Thumbnail>
        </IntroWrapper>
      </Wrapper>
    );
  }
}

export default Intro;
