import React from 'react';
import { Container } from 'components/common';
import { Wrapper, Exp } from './styles';

class Experience extends React.Component {
  render() {
    return (
      <Wrapper as={Container}>
        <Exp>
          <h1>EXPERIENCE</h1>
          <div className="step active">
            <div className="v-stepper">
              <div className="circle"></div>
              <div className="line"></div>
            </div>

            <div className="content">
              <h3>WEB PROGRAMMER</h3>
              2015 - 2016 PT. MIC Transformer Surabaya
            </div>
          </div>

          <div className="step active">
            <div className="v-stepper">
              <div className="circle"></div>
              <div className="line"></div>
            </div>

            <div className="content">
              <h3>IT TECHNICAL SUPPORT</h3>
              2016 - 2018 PT. Jivan Jaya Makmur Telecom Surabaya
            </div>
          </div>

          <div className="step active">
            <div className="v-stepper">
              <div className="circle"></div>
              <div className="line"></div>
            </div>

            <div className="content">
              <h3>FREELANCE FULLSTACK MOBILE PROGRAMMER</h3>
              2018 - 2019 Emcorp Studio
            </div>
          </div>

          <div className="step active">
            <div className="v-stepper">
              <div className="circle"></div>
              <div className="line"></div>
            </div>

            <div className="content">
              <h3>Lead Developer & Fullstack android Developer</h3>
              2019 - 2020 PT. Eklanku Indonesia Cemerlang
            </div>
          </div>

          <div className="step active">
            <div className="v-stepper">
              <div className="circle"></div>
              <div className="line"></div>
            </div>

            <div className="content">
              <h3>Android Developer</h3>
              2022 - now Tribe Agreeculture.id
            </div>
          </div>

        </Exp>
      </Wrapper>
    );
  }
}

export default Experience;
