import styled from 'styled-components';

export const Wrapper = styled.div`
  padding: 4rem 0;

  align-items: center;
  justify-content: space-between;

  @media (max-width: 960px) {
    flex-direction: column;
  }
`;

export const Details = styled.div`
  flex: 1;
  padding-right: 2rem;

  @media (max-width: 960px) {
    padding-right: unset;
    width: 100%;
    order: 1;
  }

  h1 {
    margin-bottom: 2rem;
    font-size: 26pt;
    color: #212121;
  }

  p {
    margin-bottom: 2.5rem;
    font-size: 20pt;
    font-weight: normal;
    line-height: 1.3;
    color: #707070;
  }
`;

export const Thumbnail = styled.div`
  flex: 1;

  @media (max-width: 960px) {
    width: 100%;
    margin-bottom: 2rem;
  }

  img {
    width: 100%;
  }
`;

export const Exp = styled.div`
  .container {
    width: 25%;
  }

  .step {
    padding: 10px;

    display: flex;
    flex-direction: row;
    justify-content: flex-start;

    background-color: cream;
  }

  .v-stepper {
    position: relative;
    /*   visibility: visible; */
  }

  /* regular step */
  .step .circle {
    background-color: white;
    border: 3px solid gray;
    border-radius: 100%;
    width: 20px; /* +6 for border */
    height: 20px;
    display: inline-block;
  }

  .step .line {
    top: 23px;
    left: 12px;
    /*   height: 120px; */
    height: 100%;

    position: absolute;
    border-left: 3px solid rgb(6, 150, 215);
  }

  .step.completed .circle {
    visibility: visible;
    background-color: rgb(6, 150, 215);
    border-color: rgb(6, 150, 215);
  }

  .step.completed .line {
    border-left: 3px solid rgb(6, 150, 215);
  }

  .step.active .circle {
    visibility: visible;
    border-color: rgb(6, 150, 215);
  }

  .step.empty .circle {
    visibility: hidden;
  }

  .step.empty .line {
    /*     visibility: hidden; */
    /*   height: 150%; */
    top: 0;
    height: 150%;
  }

  .step:last-child .line {
    border-left: 3px solid white;
    z-index: -1; /* behind the circle to completely hide */
  }

  .content {
    margin-left: 20px;
    display: inline-block;
  }

  /* codepen override */
  html * {
    font-size: 15px !important;
    color: #000 !important;
    font-family: Arial !important;
  }
`;
