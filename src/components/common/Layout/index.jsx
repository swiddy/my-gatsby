import React from 'react';
import { Footer } from 'components/theme';
import { Global } from './styles';
import './fonts.css';

// eslint-disable-next-line react/prop-types
export const Layout = ({ children }) => (
  <>
    <Global />
    {children}
    <Footer />
  </>
);
